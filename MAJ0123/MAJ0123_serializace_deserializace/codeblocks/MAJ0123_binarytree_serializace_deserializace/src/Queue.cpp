#include "Queue.h"

/**
 * @file Queue.cpp
 * @author Lukas Majoros
 * @date 20 Dubna 2018
 * @brief cpp soubor tridy Queue
 *
 * cpp soubor tridy Queue,
 */


 /****************************************************************************
 *   Prevazne ADSLibrary od Dvorskeho, misto intu BinaryTree Node, FIFO     *
 *   Pridavani do fronty je Enqueue a odebrani je Dequeue .                 *
 ****************************************************************************/

/**
 * @brief Konstruktor
 */
Queue::Queue() {
	Head = nullptr;
	Tail = nullptr;
}

/**
 * @brief Destruktor
 */
Queue::~Queue() {
	Clear();
}



/**
 * @brief Funkce Engueue
 *
 * pridavani do fronty
 */
void Queue::Enqueue(Node * node) {
	Item * item = new Item();
	item->Node = node;
	item->Next = nullptr;

	if (IsEmpty())
		Head = item;
	else
		Tail->Next = item;

	Tail = item;
}

/**
 * @brief Funkce Dequeue
 *
 * Odebrani z fronty
 */
Node * Queue::Dequeue() {
	Item * item = Head;
	Node * result = item->Node;

	Head = Head->Next;

	if (IsEmpty())
		Tail = nullptr;

	delete item;
	return result;
}

/**
 * @brief Funkce Peek
 */
Node * Queue::Peek() {
	return Head->Node;
}

/**
 * @brief Funkce IsEmpty
 *
 * Vynulovani hlavicky
 */
bool Queue::IsEmpty() {
	return Head == nullptr;
}

/**
 * @brief Funkce Clear
 *
 * vymazani fronty
 */
void Queue::Clear() {
	while (!IsEmpty())
		Dequeue();
}

