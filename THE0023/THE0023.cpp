// Binary Search Tree - Implemenation in C++
// Simple program to create a BST of integers and search an element in it 
#include "stdafx.h"
#include<iostream>
#include <ctime>
#include <random>
#include <stack>
#include <fstream>
#include <vector>
#include <string>
using namespace std;
struct BstNode 
{
	int data;
	BstNode* left;
	BstNode* right;
};

BstNode* GetNewNode(int data) 
{
	BstNode* newNode = new BstNode();
	newNode->data = data;
	newNode->left = newNode->right = NULL;
	return newNode;
}
 
BstNode* Insert(BstNode* root, int data) {
	if (root == NULL) 
	{
		root = GetNewNode(data);
	}
	
	else if (data <= root->data) 
	{
		root->left = Insert(root->left, data);
	} 
	else 
	{
		root->right = Insert(root->right, data);
	}
	return root;
}

//bool Search(BstNode* root, int data) {
//	if (root == NULL) {
//		return false;
//	}
//	else if (root->data == data) {
//		return true;
//	}
//	else if (data <= root->data) {
//		return Search(root->left, data);
//	}
//	else {
//		return Search(root->right, data);
//	}
//}
//Serializace
stack<BstNode*> myStack;
BstNode* tmp = NULL;
vector<string> serializovanyStrom;
string temp;
BstNode* Serializace(BstNode* root) 
{
	
	if (root == NULL) 
	{
		if (!myStack.empty())
		{
			tmp = myStack.top();
			myStack.pop();
			//cout << "x" << "  ";
			serializovanyStrom.push_back("x");
			//cout << tmp->data << endl;
			Serializace(tmp->right);
			//myStack.pop();
		}			
	}
	else
	{
		myStack.push(root);
		//cout << root->data << "  ";
		temp = to_string(root->data);
		serializovanyStrom.push_back(temp);
		//cout << myStack.top()->data << endl;
		Serializace(root->left);
	}
	return root;
}
	
void BSTOutputFile(string fileName)
{
	ofstream ofs;
	ofs.open(fileName);
	cout << "Serializovany BST:" << endl;
	for (int i = 0; i < serializovanyStrom.size(); i++)
	{
		ofs << serializovanyStrom[i] << ",";
		cout << serializovanyStrom[i] << "  ";
	}
	cout << endl;
	ofs.close();
	
}
vector<string> inputBST;
BstNode* Deserializace(string fileName,BstNode* rootDes)
{
	string key;
	ifstream ifs;

	ifs.open(fileName);
	while (!ifs.eof())
	{
		getline(ifs, key, ',');
		inputBST.push_back(key);
	}
	ifs.close();
	/*cout << "Precteny serializovany BST ze souboru" << endl;
	for (int i = 0; i < inputBST.size(); i++)
	{
		cout << inputBST[i] << "  ";
	}*/
	
	
	cout << "Deserializovany BST:" << endl;
	for (int i = 0; i < inputBST.size(); i++)
	{
		if (inputBST[i]!="x")
		{
			try {
				rootDes = Insert(rootDes, stoi(inputBST[i]));
				cout << stoi(inputBST[i]) << "  ";
			}
			catch (std::invalid_argument& e) {
				// if no conversion could be performed
			}
			catch (std::out_of_range& e) {
				
			}
			catch (...) {
			// everything else
			}
		}
		
	}
	return rootDes;
}
BstNode* RandomBST(BstNode* root)
{
	srand(time(NULL));
	//BstNode* root = NULL;
	int j = 0;
	int min = 0;
	int max = 100;
	cout << "Klice do BST:" << endl;
	for (int i = 0; i < 10; i++)
	{
		//j = rand();
		j = min + (rand() % static_cast<int>(max - min + 1));
		cout << j << "  ";
		root = Insert(root, j);
	}
	cout << endl << endl << "Klic korenoveho uzlu:	" << root->data << endl;
	return root;
}

bool isEqual(BstNode* root, BstNode* rootDes)
{

	if (root == NULL && rootDes == NULL)
	{
		return true;
	}
	if ((root == NULL && rootDes != NULL) || (root != NULL && rootDes == NULL))
	{
		return false;
	}
	if ((root->data) != (rootDes->data))
	{
		return false;
	}
	return (isEqual(root->left, rootDes->left) &&
		isEqual(root->right, rootDes->right));
}

int main() {
	BstNode* root = NULL;
	BstNode* rootDes = NULL;
	string inputFileName;
	string outputFileName;
	int choice;
	do
	{

		cout << endl << endl;
		cout << "1	Vytvoreni noveho BST" << endl;
		cout << "2	Serializace" << endl;
		cout << "3	Deserializace" << endl;
		cout << "4	Kontrola" << endl;
		cout << "5	Konec" << endl;
		cout << endl;
		cin >> choice;
		switch (choice)
		{
		case 1:
		
			root=RandomBST(root);
			break;
		case 2:
			root=Serializace(root);
			cout << "Zadejte nazev vystupniho souboru:" << endl;
			cin >> outputFileName;
			BSTOutputFile(outputFileName);
			break;
		case 3:
			cout << "Zadejte nazev souboru:" << endl;
			cin >> inputFileName;
			rootDes=Deserializace(inputFileName,rootDes);
			break;
		case 4:
			if (isEqual(root, rootDes) == true)
			{
				cout << endl << "Puvodni BST = Deserializovany BST" << endl << endl;
			}
			else
			{
				cout << endl << "Puvodni BST a Deserializovany BST jsou rozdilne!!!" << endl << endl;
			}
			break;
		default:
			break;
		}
	} while (choice != 5);
		
	return 0;
}