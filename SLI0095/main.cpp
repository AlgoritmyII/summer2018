#include <iostream>
#include <string>
#include <functional>

#define DEFAULT_TABLE_SIZE 13

using namespace std;

class HashTable
{
private:
    string *HashTab;
    int TabSize;
public:
    HashTable();
    HashTable(int Size);
    bool Insert(string s);
    bool Search(string s);
    int Count();
    double LoadFactor();
    void Report();
};
HashTable::HashTable()
{
    HashTab = new string[DEFAULT_TABLE_SIZE];
    TabSize = DEFAULT_TABLE_SIZE;
    for(int i = 0; i < TabSize; i++)
    {
        HashTab[i] = "-1";
    }
}
HashTable::HashTable(int Size)
{
    HashTab = new string[Size];
    TabSize = Size;
    for(int i = 0; i < TabSize; i++)
    {
        HashTab[i] = "-1";
    }
}
bool HashTable::Insert(string s)
{
    int i = 0;
    hash<string> hashing;
    if(this->Count() == TabSize)
    {
        return false;
    }
    else
    {
        while(true)
        {
            if(HashTab[(hashing(s) + i*(11 - hashing(s)))%TabSize] == "-1")
            {
                HashTab[(hashing(s) + i*(11 - hashing(s)))%TabSize] = s;
                return true;
            }
            else
            {
                i++;
            }
        }
    }
}
bool HashTable::Search(string s)
{
    int i = 0;
    hash<string> hashing;
    while(i < TabSize)
    {
        if(HashTab[(hashing(s) + i*(11 - hashing(s)))%TabSize] == "-1")
            return false;
        if(HashTab[(hashing(s) + i*(11 - hashing(s)))%TabSize] == s)
            return true;
        i++;
    }
    return false;
}
int HashTable::Count()
{
    int k = 0;
    for(int i = 0; i < TabSize; i++)
    {
        if(HashTab[i] != "-1")
        {
            k++;
        }
    }
    return k;
}
double HashTable::LoadFactor()
{
    return (double)Count()/(double)TabSize;
}
void HashTable::Report()
{
    for(int i = 0; i < TabSize; i++)
    {
        cout << i << '\t';
        if(HashTab[i] != "-1")
        {
            cout << "--->" << HashTab[i];
        }
        cout << endl;
    }
    cout << endl;
}


int main()
{
    HashTable *A = new HashTable(17);
    cout << A->Insert("Ahoj") << endl;
    cout << A->Insert("Mobil") << endl;
    cout << A->Insert("Cau") << endl;
    cout << A->Insert("Hash") << endl;
    cout << A->Insert("Houba") << endl;
    cout << A->Insert("Les") << endl;
    cout << A->Insert("Fletna") << endl;
    cout << A->Insert("Tricko") << endl;
    cout << A->Insert("Housle") << endl;
    cout << endl;
    A->Report();
    cout << A->Count() << endl;
    cout << A->LoadFactor() << endl;
    cout << endl;
    cout << A->Insert("Kocka") << endl;
    cout << A->Insert("Boty") << endl;
    cout << A->Insert("Jelen") << endl;
    cout << A->Insert("Motor") << endl;
    cout << A->Insert("Internet") << endl;
    cout << A->Insert("Knedlik") << endl;
    cout << A->Insert("Rotor") << endl;
    cout << A->Insert("Pes") << endl;
    cout << endl;
    A->Report();
    cout << A->Search("Louka")<< endl;
    cout << A->Search("Housle") << endl;
    cout << endl;
    cout << A->Count() << endl;
    cout << A->LoadFactor() << endl;
    return 0;
}
